// Package filer provides ...
package s3Filer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	result := init()
	assert.NotEmpty(t, result.AccessKey)
	assert.NotEmpty(t, result.SecretAccessKey)
	assert.NotEmpty(t, result.Region)
	assert.NotEmpty(t, result.BucketName)
}

func TestUpload(t *testing.T) {
	token := init()
	err := token.Upload()
	assert.NoError(t, err)
}

//main function
func TestDownload(t *testing.T) {
	token := init()
	err := token.Download()

	assert.NoError(t, err)
}
