// Package main provides ...
package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"github.com/kelseyhightower/envconfig"
	"github.com/urfave/cli"
)

type AwsToken struct {
	AccessKey       string
	SecretAccessKey string
	Region          string `default:"ap-northeast-1"`
	BucketName      string `default:"rabbit-can-dev"`
}

func main() {
	app := cli.NewApp()
	app.Name = "AWS-S3-CLI"
	app.Usage = "This app AWS on S3 Command Line Intaerface tool kit"
	app.Version = "1.0.0"

	app.Commands = []cli.Command{
		{
			Name:    "upload",
			Aliases: []string{"u"},
			Usage:   "This is a command to upload a file to S3",
			Action: func(c *cli.Context) error {
				// fmt.Println("Upload: ", c)
				awsS3 := initial()
				err := awsS3.upload(c.Args().First())
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
		{
			Name:    "download",
			Aliases: []string{"d"},
			Usage:   "This is a command to download a file to S3",
			Action: func(c *cli.Context) error {
				awsS3 := initial()
				err := awsS3.download()
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
	}

	app.Run(os.Args)
}

func initial() (config AwsToken) {
	envconfig.Process("AWS", &config)
	return
}

func (token *AwsToken) upload(filename string) (err error) {
	var t string
	creds := credentials.NewStaticCredentials(token.AccessKey, token.SecretAccessKey, t)
	_, err = creds.Get()
	if err != nil {
		fmt.Printf("bad credentials: %s", err)
	}
	cfg := aws.NewConfig().WithRegion(token.Region).WithCredentials(creds)
	svc := s3.New(session.New(), cfg)

	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("err opening file: %s", err)
	}
	defer file.Close()
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer

	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	path := "/media/" + file.Name()
	params := &s3.PutObjectInput{
		Bucket:        aws.String(token.BucketName),
		Key:           aws.String(path),
		Body:          fileBytes,
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
	}
	resp, err := svc.PutObject(params)
	if err != nil {
		fmt.Printf("bad response: %s", err)
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
	return nil
}

func (token *AwsToken) download() (err error) {
	var t string
	creds := credentials.NewStaticCredentials(token.AccessKey, token.SecretAccessKey, t)
	sess := session.Must(session.NewSession(&aws.Config{
		Region:      aws.String(token.Region),
		Credentials: creds,
	}))

	//===== create output file
	outputFile := "main.rb"
	file, err := os.Create(outputFile)
	if err != nil {
		return
	}
	defer file.Close()

	//===== download from s3
	downloader := s3manager.NewDownloader(sess)
	_, err = downloader.Download(file, &s3.GetObjectInput{
		Bucket: aws.String(token.BucketName),
		Key:    aws.String("media/test/main.rb"),
	})
	if err != nil {
		// os.Remove(outputFile)
		return
	}
	return

}
